package basics.search;

public class LinearSearch {

	public static void main(String args[]) {

		int[] numArray = { 10, 34, 45, 67, 78 };
		int searchKey = 67;

		System.out.println("Key : " + searchKey + " is at index " + search(numArray, searchKey));
	}

	public static int search(int[] arr, int key) {

		int size = arr.length;

		for (int i = 0; i < size; i++) {
			if (arr[i] == key) {
				return i;
			}
		}
		return -1;
	}
}
