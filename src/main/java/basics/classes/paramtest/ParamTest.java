package basics.classes.paramtest;

public class ParamTest {

	public static void main(String[] args) {
		//Methods cant modify numeric parameters
		System.out.println("Testing triple value");
		double percent = 10;
		System.out.println("Before:percent = " + percent);
		tripleValue(percent);
		System.out.println("After:percent =  " + percent);
		
		//Methods can change state of object
		System.out.println("\nTesting tripleSalary");
		Employee imtiaz = new Employee("Imtiaz", 50000);
		System.out.println("Before: tripleSalary " + imtiaz.getSalary());
		tripleSalary(imtiaz);
		System.out.println("After: tripleSalary " + imtiaz.getSalary());
		
		System.out.println("-------Testing Swap------");
		Employee a = new Employee("Adil", 10000);
		Employee b = new Employee ("Saqib", 20000);
		System.out.println(" Values of a  before Swap :" + a.getName() + " " + a.getSalary()
		+ " Values of b before Swap :" + b.getName() + " " + b.getSalary());		
		swap(a, b);
		System.out.println(" Values of a after Swap :" + a.getName() + " " + a.getSalary() 
				+ " Values of b after Swap :" + b.getName() + " " + b.getSalary());
		
		
	}
	public static void swap(Employee a, Employee b) {
		Employee temp;
		temp = a;
		a = b;
		b = temp;
		System.out.println(" Values of a  inside swap :" + a.getName() + " " + a.getSalary() 
				+ "  Values of  b inside Swap :" + " " + b.getName() + " " + b.getSalary());
		
		
	}
	public static void tripleValue(double x) {
		x = 3 * x;
		System.out.println("Value of x in tripleValue = " + x);
	}
	public static void tripleSalary(Employee x) {
		x.raiseSalary(200);;
		System.out.println("Value of x in tripleValue = " + x);
	}

}
class Employee {
	private String name;
	private double salary;
	
	public Employee(String name, double salary) {
		this.name = name;
		this.salary = salary;
	}
	
	public String getName() {
		return name;
	}
	
	public double getSalary() {
		return salary;
	}
	public void raiseSalary(double salary) {
		double raise = this.salary * salary / 100;
		this.salary = raise;
	}
		
}

