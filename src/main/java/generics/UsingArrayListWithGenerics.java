package generics;

import java.util.ArrayList;

public class UsingArrayListWithGenerics {

    public static void main(String [] args){
        ArrayList<String> names = new ArrayList<>();
        names.add("One");
        names.add("Two");
        names.add("Three");
        System.out.println(concat(names));
    }

    public static String concat(ArrayList<String> strings){
        StringBuilder sb = new StringBuilder();
        for(String str: strings){
            sb.append(str);
        }
        return sb.toString();
    }

}
