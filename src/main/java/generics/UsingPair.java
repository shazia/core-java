package generics;

import java.util.Arrays;
import java.util.List;

public class UsingPair {

    public static void main(String[] args) {
        List<String> words = Arrays.asList("Mary", "had", "a", "little", "lamb");
        Pair<String> minMaxLength = ArrayAlg.minMax(words);
        System.out.println(minMaxLength);

        List<Double> numbers = Arrays.asList(10.5, 50.5, 40.0, 100.1, 11.6);
        Pair<Double> minMaxNumbers = ArrayAlg.minMax(numbers);
        System.out.println(minMaxNumbers);
    }

    public static class ArrayAlg {

        public static <T extends Comparable<T>> Pair<T> minMax(List<T> list) {
            if (list.size() == 0) {
                return null;
            } else {
                T min = list.get(0);
                T max = list.get(0);
                for (int i = 1; i < list.size(); i++) {
                    if (min.compareTo(list.get(i)) > 0) {
                        min = list.get(i);
                    }

                    if (max.compareTo(list.get(i)) < 0) {
                        max = list.get(i);
                    }
                }

                return new Pair<>(min, max);
            }
        }
    }

}
