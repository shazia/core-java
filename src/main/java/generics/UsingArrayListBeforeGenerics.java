package generics;

import java.util.ArrayList;

public class UsingArrayListBeforeGenerics {

    public static void main(String [] args){
        ArrayList names = new ArrayList();
        names.add("One");
        names.add("Two");
        names.add(3);

        //concat(names);
    }

    private static String concat(ArrayList strings){
        StringBuilder sb = new StringBuilder();
        for(Object obj: strings){
            sb.append((String)obj);
            sb.append(" ");
        }
        return sb.toString();
    }
}
