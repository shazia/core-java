package generics;

public class UsingGenerics {

    public static <T> String asStr(T x){
        return x.toString();
    }

    public static <T> boolean areEqual(T x, T y){
        return x.equals(y);
    }

    public static <T extends Number> boolean lessThan(T x, T y){
        return x.doubleValue() < y.doubleValue();
    }

    public static boolean lessThanWithoutGenerics(Object x, Object y){
        Number xNum = (Number)x;
        Number yNum = (Number)y;

        return xNum.doubleValue() < yNum.doubleValue();
    }

    public static void main(String [] args){
        assert(asStr(1).equals("1"));
        assert(asStr("one").equals("one"));
        assert(lessThan(10, 20));
        assert(lessThanWithoutGenerics(30, 40));
    }
}
