package generics;

import java.time.Duration;
import java.time.LocalDate;

public class GenericsErasure {

    public static class Pair<T> {
        private T first;
        private T second;

        public T getFirst() {
            return first;
        }

        public void setFirst(T first) {
            this.first = first;
        }

        public T getSecond() {
            return second;
        }

        public void setSecond(T second) {
            this.second = second;
        }

        public Pair(T first, T second) {
            this.first = first;
            this.second = second;
        }
    }

    public static class DateInterval extends Pair<LocalDate>{

        public DateInterval(LocalDate first, LocalDate second) {
            super(first, second);
        }

        @Override
        public void setSecond(LocalDate second){
            if(second.isAfter(getFirst())){
                super.setSecond(second);
            }
        }

    }

    public static void main(String [] args){
        DateInterval dateInterval = new DateInterval(LocalDate.now(), LocalDate.now().plus(Duration.ofDays(1)));
        dateInterval.setSecond(LocalDate.now());
        System.out.println(dateInterval);
    }
}
