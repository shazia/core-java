package proxies;

public class Walker {

    @IsARunner
    public void walk(){
        System.out.println("I am not a runner. I am a walker!");
    }

}
