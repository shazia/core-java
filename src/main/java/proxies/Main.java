package proxies;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {
        Runner runner = new SimpleImplementationRunner();
        Runner walker = proxyWalkerToRunner();
        System.out.println("I just created a park!. Now I will ask runners to run");
        runner.run();
        walker.run();
        System.out.println("Just destroyed the park!");
    }

    private static Runner proxyWalkerToRunner() {
        return (Runner) Proxy.newProxyInstance(Main.class.getClassLoader(), new Class<?>[]{Runner.class},
                makeNotARunnerInvocationHandler());
    }

    private static InvocationHandler makeNotARunnerInvocationHandler() {
        Walker walker = new Walker();

        return new InvocationHandler() {

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if(method.isAnnotationPresent(IsARunner.class)){
                    walker.walk();
                }

                return proxy;
            }
        };
    }
}
