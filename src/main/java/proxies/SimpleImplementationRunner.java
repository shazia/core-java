package proxies;

public class SimpleImplementationRunner implements Runner {
    @Override
    public void run() {
        System.out.println("I am a simple runner");
    }
}
